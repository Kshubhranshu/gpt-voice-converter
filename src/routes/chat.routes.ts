import { Router } from "express";
import { chatWithGpt } from "../controllers/chat.controllers";
 
const chatRouter = Router();

chatRouter.post("/chat",chatWithGpt)

export default chatRouter;