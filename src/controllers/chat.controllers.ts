import { Request,Response } from "express";
import { Configuration, OpenAIApi } from "openai";

export const chatWithGpt = async (req: Request, res: Response) => {
    try {
      const { message } = req.body;
      const key =process.env.OPEN_API_KEY

      const configuration = new Configuration({
        apiKey: key,
      });
      const openai = new OpenAIApi(configuration);

      const chatCompletion = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        messages: [{role: "user", content: message}],
      });

      return res.status(200).send({answer: chatCompletion.data.choices[0].message})
    } catch (error:any) {
      console.error('Error:', error.message);
      res.status(500).json({ error: 'An error occurred' });
    }
};

