import express from 'express';
import swaggerUi from 'swagger-ui-express';
const swaggerJson = require('./src/utils/swagger.json');
import cors from 'cors';
import { corsFunction } from './src/utils/cors.utils';
import chatRouter from './src/routes/chat.routes';

const app = express();
const router = express.Router();

app.use(cors())

app
  .use(corsFunction)
  .use(express.json())
  .use(express.urlencoded({ extended: true }))
  .use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerJson))
  .use("/api/v1",chatRouter)
  .use('/health', (_req: express.Request, res: express.Response) => {
    res.status(200).send({server: "Gpt Voice Converter Backaned", time: new Date(),  status: "active"});
  })
  .use('*', (req, res) => {
    res.status(200).send('Route not found');
  });

export default app;